<?php

namespace App\JsonStreamingParser;

use JsonStreamingParser\Listener\ListenerInterface;

interface ReaderInterface
{
    /**
     * Read data by stream from json
     *
     * @param ListenerInterface $listener
     * @param string $filePath
     */
    public function read(ListenerInterface $listener, string $filePath): void;
}