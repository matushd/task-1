<?php

namespace App\JsonStreamingParser;

use JsonStreamingParser\Listener\ListenerInterface;

class Listener implements ListenerInterface
{
    public const MAX_LEVEL_INFINITY = -1;

    /**
     * @var callable
     */
    protected $callback;

    protected int $level;

    protected int $maxLevel;

    protected array $stack;

    protected array $keys;

    protected array $json;

    /**
     * Listener
     *
     * @param callable $callback
     * @param int $maxLevel
     */
    public function __construct(callable $callback, int $maxLevel = self::MAX_LEVEL_INFINITY)
    {
        $this->callback = $callback;
        $this->maxLevel = $maxLevel;
    }

    /**
     * @return array
     */
    public function getJson(): array
    {
        return $this->json;
    }

    /**
     * @inheritDoc
     */
    public function startDocument(): void
    {
        $this->stack = [];
        $this->level = 0;
        $this->keys = [];
    }

    /**
     * @inheritDoc
     */
    public function endDocument(): void
    {
        // TODO: Implement endDocument() method.
    }

    /**
     * @inheritDoc
     */
    public function startObject(): void
    {
        $this->level++;
        $this->stack[] = [];
    }

    /**
     * @inheritDoc
     */
    public function endObject(): void
    {
        $this->level--;
        $obj = array_pop($this->stack);
        if (empty($this->stack)) {
            $this->json = $obj;
        } else {
            $this->value($obj);
        }
    }

    /**
     * @inheritDoc
     */
    public function startArray(): void
    {
        $this->startObject();
    }

    /**
     * @inheritDoc
     */
    public function endArray(): void
    {
        $this->endObject();
    }

    /**
     * @inheritDoc
     */
    public function key(string $key): void
    {
        $this->keys[$this->level] = $key;
    }

    /**
     * @inheritDoc
     */
    public function value($value)
    {
        $obj = array_pop($this->stack);
        if (!empty($this->keys[$this->level])) {
            $obj[$this->keys[$this->level]] = $value;
            $this->keys[$this->level] = null;
        } else {
            $obj[] = $value;
        }

        if (isset($obj['children']) && !$obj['children'] && $this->isLevelValid()) {
            $obj['name'] = call_user_func($this->callback, $obj);
        }

        $this->stack[] = $obj;
    }

    /**
     * @inheritDoc
     */
    public function whitespace(string $whitespace): void
    {
        // TODO: Implement whitespace() method.
    }

    /**
     * @inheritDoc
     */
    private function isLevelValid(): bool
    {
        return $this->maxLevel < 1 || $this->level <= $this->maxLevel;
    }
}
