<?php

namespace App\JsonStreamingParser;

use JsonStreamingParser\Listener\ListenerInterface;
use JsonStreamingParser\Parser;

class Reader implements ReaderInterface
{
    /**
     * @inheritDoc
     */
    public function read(ListenerInterface $listener, string $filePath): void
    {
        $stream = fopen($filePath, 'r');
        try {
            $parser = new Parser($stream, $listener);
            $parser->parse();
            fclose($stream);
        } catch (Exception $e) {
            fclose($stream);
            throw $e;
        }
    }
}