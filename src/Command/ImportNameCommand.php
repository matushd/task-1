<?php

namespace App\Command;

use App\Category\Repository\Builder;
use App\JsonStreamingParser\Listener;
use App\JsonStreamingParser\Reader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ImportNameCommand extends Command
{
    protected static $defaultName = 'app:import-name';

    /**
     * @inheritDoc
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Find name from list file and inject into tree items')
            ->setHelp('This command allows you to find name in list file and inject into each leaf from tree file')
            ->addOption('list', 'l', InputOption::VALUE_REQUIRED, 'List file path')
            ->addOption('tree', 't', InputOption::VALUE_REQUIRED, 'Tree file path')
            ->addOption('maxDepth', 'm', InputOption::VALUE_OPTIONAL, 'Max depth to parse tree', Listener::MAX_LEVEL_INFINITY);
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $listPath = $this->preparePath($input, 'list');
        $treePath = $this->preparePath($input, 'tree');
        $maxDepth = (int)$input->getOption('maxDepth');
        $reader = new Reader();
        $repositoryBuilder = new Builder($reader);
        $repository = $repositoryBuilder->createRepositoryByDataSource($listPath);
        $callback = function($object) use ($output, $repository) {
            $object['name'] = $repository->findNameById($object['id']);
            $output->writeln(json_encode($object));
        };

        $listener = new Listener($callback, $maxDepth);
        $reader->read($listener, $treePath);
        return 0;
    }

    /**
     * Prepare input path
     *
     * @param InputInterface $input
     * @param string $option
     *
     * @return string
     */
    private function preparePath(InputInterface $input, string $option): string
    {
        $path = __DIR__  . '/../../' . $input->getOption($option);
        if (!file_exists($path) || !is_readable($path)) {
            throw new \InvalidArgumentException(
                sprintf('File %s does not exists or is not readable', $path)
            );
        }

        return $path;
    }
}
