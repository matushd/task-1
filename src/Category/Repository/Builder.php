<?php

namespace App\Category\Repository;

use App\Category\Repository;
use App\Category\JsonStreamingParser\Listener;
use App\JsonStreamingParser\ReaderInterface;

class Builder
{
    private $reader;

    /**
     * Builder
     *
     * @param ReaderInterface $reader
     */
    public function __construct(ReaderInterface $reader)
    {
        $this->reader = $reader;
    }

    /**
     * Create category repository by data source
     *
     * @param string $filePath
     *
     * @return Repository
     */
    public function createRepositoryByDataSource(string $filePath): Repository
    {
        $storage = [];
        $callback = function ($value) use (&$storage)
        {
            $id = $value['category_id'];
            foreach ($value['translations'] as $lang => $config) {
                $storage[$id][$lang] = $config['name'];
            }
        };
        $listener = new Listener($callback);
        $this->reader->read($listener, $filePath);
        return new Repository($storage);
    }
}