<?php

namespace App\Category\JsonStreamingParser;

use JsonStreamingParser\Listener\ListenerInterface;

class Listener implements ListenerInterface
{
    /**
     * @var callable
     */
    protected $callback;

    protected int $level;

    protected array $stack;

    protected array $keys;

    protected array $json;

    /**
     * Listener
     *
     * @param callable $callback
     */
    public function __construct(callable $callback)
    {
        $this->callback = $callback;
    }

    /**
     * @return array
     */
    public function getJson(): array
    {
        return $this->json;
    }

    /**
     * @inheritDoc
     */
    public function startDocument(): void
    {
        $this->stack = [];
        $this->level = 0;
        $this->keys = [];
    }

    /**
     * @inheritDoc
     */
    public function endDocument(): void
    {
        // TODO: Implement endDocument() method.
    }

    /**
     * @inheritDoc
     */
    public function startObject(): void
    {
        $this->level++;
        $this->stack[] = [];
    }

    /**
     * @inheritDoc
     */
    public function endObject(): void
    {
        $this->level--;
        $obj = array_pop($this->stack);
        if (empty($this->stack)) {
            $this->json = $obj;
        } else {
            $this->value($obj);
        }
    }

    /**
     * @inheritDoc
     */
    public function startArray(): void
    {
        $this->startObject();
    }

    /**
     * @inheritDoc
     */
    public function endArray(): void
    {
        $this->endObject();
    }

    /**
     * @inheritDoc
     */
    public function key(string $key): void
    {
        $this->keys[$this->level] = $key;
    }

    /**
     * @inheritDoc
     */
    public function value($value): void
    {
        $obj = array_pop($this->stack);
        if (!empty($this->keys[$this->level])) {
            $obj[$this->keys[$this->level]] = $value;
            $this->keys[$this->level] = null;
        } else {
            $obj[] = $value;
        }

        if (!empty($obj['translations'])) {
            call_user_func($this->callback, $obj);
        }

        $this->stack[] = $obj;
    }

    /**
     * @inheritDoc
     */
    public function whitespace(string $whitespace): void
    {
        // TODO: Implement whitespace() method.
    }
}
