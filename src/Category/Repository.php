<?php

namespace App\Category;

class Repository
{
    private array $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function findNameById(int $id, string $default = '', string $lang = 'pl_PL'):? string
    {
        return $this->data[$id][$lang] ?? $default;
    }
}