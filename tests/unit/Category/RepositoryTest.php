<?php

namespace App\Category;


class RepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var Repository
     */
    private $repository;

    protected function _setUp(): void
    {
        $storage = [
            12 => [
                'pl_PL' => 'Witojcie',
            ],
        ];
        $this->repository = new Repository($storage);
    }

    protected function _tearDown(): void
    {
        unset($this->repository);
    }


    public function testFindNameById(): void
    {
        $this->assertEquals('Witojcie', $this->repository->findNameById(12));
    }

    public function testFindNameByIdNotFound(): void
    {
        $this->assertEmpty($this->repository->findNameById(11));
        $this->assertEmpty($this->repository->findNameById(12, '', 'en_EN'));
    }
}
