<?php

namespace App\Category\Repository;


use App\Category\Repository;
use App\JsonStreamingParser\ReaderInterface;

class BuilderTest extends \Codeception\Test\Unit
{
    public function testCreateRepositoryByDataSource(): void
    {
        $reader = $this->makeEmpty(ReaderInterface::class);
        $builder = new Builder($reader);
        $repository = $builder->createRepositoryByDataSource('/');
        $this->assertInstanceOf(Repository::class, $repository);
    }
}
