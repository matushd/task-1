# Task 1
Write a program which is adding name property into each leaf of given tree
structure from tree.json file with name from list.json file. You should correlate
structures through category_id from list.json and Id in tree.json.

The output should be similar to:
```
[
...
{“id”:19”, “name”:”Zdrowie i uroda”,”children”:[]},
...
]
```

Things to consider in your implementation
* Tree nesting level - in some cases nesting level could be really deep
* Testability

## Init app
```
composer install
```
## How to run
```
php bin/console app:import-name --list=data/list.json --tree=data/tree.json
```
* list - source list file
* tree - source tree file
* maxDepth - max depth of leaf
## Unit tests
```
php vendor/bin/codecept run unit
```
## Key libraries
```
salsify/json-streaming-parser
```
This is a simple, streaming parser for processing large JSON documents. Use it for parsing very large JSON documents to avoid loading the entire thing into memory, which is how just about every other JSON parser for PHP works.